package com.producer.Producerdemo.controller;

import com.producer.Producerdemo.dto.Employee;
import com.producer.Producerdemo.repo.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class EmpProducerController {

    @Autowired
    private EmployeeRepo emp;
    @GetMapping("/getEmpDetails")
    public Map<Integer, Employee> getAllEmployee()
    {
        return emp.getallEmp();
    }
}
