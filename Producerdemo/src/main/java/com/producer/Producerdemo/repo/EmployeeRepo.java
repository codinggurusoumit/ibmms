package com.producer.Producerdemo.repo;

import com.producer.Producerdemo.dto.Employee;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeRepo {
    static Map<Integer, Employee> employeeMap;
    static {
       employeeMap=new HashMap<>();
       Employee emp=new Employee();
        emp.setEmpName("soumit Ghosh");
        emp.setEmail("soumit.ghosh@gmail.com");
        emp.setPassword("soumit123");
        emp.setId(123);
        employeeMap.put(emp.getId(),emp);
        emp.setEmpName("swarnali Ghosh");
        emp.setEmail("swarnali.ghosh@gmail.com");
        emp.setPassword("swarnali321");
        emp.setId(321);
        employeeMap.put(emp.getId(),emp);

    }
    public  Map<Integer,Employee> getallEmp()
    {
        return employeeMap;
    }


}
