package com.consumer.ConsumerDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ConsumerController {

    @Autowired
    PostmanEchoClient client;
    @GetMapping("/getemps")
    public Map<Integer, Employee> getEmpDetails()
    {
        return client.getAllEmployee();
    }
}
