package com.consumer.ConsumerDemo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name = "producer")
public interface PostmanEchoClient {
    @GetMapping("/getEmpDetails")
    public Map<Integer, Employee> getAllEmployee();
}
