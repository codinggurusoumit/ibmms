package com.consumer.ConsumerDemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootTest
@EnableFeignClients
class ConsumerDemoApplicationTests {

	@Test
	void contextLoads() {
	}

}
